class logcheck::remove {

  package { [ 'logcheck', 'logcheck-database' ]:
    ensure => absent
  }

}
